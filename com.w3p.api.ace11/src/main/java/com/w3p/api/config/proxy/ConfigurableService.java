package com.w3p.api.config.proxy;

import java.util.Properties;

import com.w3p.api.config.model.PolicyModel;

public class ConfigurableService {
	
	private final BrokerProxy intNodeProxy;
	private final PolicyModel policyModel;
	
	private final Properties props = new Properties();


	public ConfigurableService(BrokerProxy intNodeProxy, PolicyModel policyModel) {
		this.intNodeProxy = intNodeProxy;
		this.policyModel = policyModel;
	}
	
	public PolicyModel getPolicyModel() {
		return policyModel;
	}
	
	public void delete() throws ConfigManagerProxyLoggedException, ConfigManagerProxyPropertyNotInitializedException {
		intNodeProxy.deleteConfigurableService(policyModel.getServiceType(), policyModel.getServiceName());
//		getConfigurableService(policyModel.getServiceType(), policyModel.getServiceName()).delete();
	}
	
	public void setProperty(String name, String value) {
		props.put(name, value);		
	}
	
	public String getProperty(String name) {
		return (String)props.get(name);		
	}
}
