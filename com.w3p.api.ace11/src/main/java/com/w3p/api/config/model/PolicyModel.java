package com.w3p.api.config.model;

public class PolicyModel extends AbstractModel {
	
	private final String serviceType;
	private final String serviceName;
	
	
	public PolicyModel(String serviceType, String serviceName) {
		this.serviceType = serviceType;
		this.serviceName = serviceName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public String getServiceName() {
		return serviceName;
	}
}
