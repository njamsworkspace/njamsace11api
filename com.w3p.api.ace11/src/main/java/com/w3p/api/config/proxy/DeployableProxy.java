package com.w3p.api.config.proxy;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.w3p.api.config.model.DeployableModel;
import com.w3p.api.config.model.MsgflowModel;
import com.w3p.api.config.model.StaticLibModel;
import com.w3p.api.config.model.SubflowModel;


public class DeployableProxy extends AdministeredObject {
	
	protected DeployableProxy intSvrProxy;
	protected DeployableModel deployableModel;
	
	// Message Flows
	private final List<MessageFlowProxy> msgflowProxies = new LinkedList<>();
	//Sub Flows
	private final List<SubFlowProxy> subflowProxies = new LinkedList<>();
	//Static Libs
	private final List<StaticLibraryProxy> staticLibProxies = new LinkedList<>();
	
	

	public DeployableProxy() {
		super();
	}

	public DeployableProxy(DeployableProxy intSvrProxy, DeployableModel deployableModel) {
		this.intSvrProxy = intSvrProxy;
		this.deployableModel = deployableModel;
	}

	@Override
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		return deployableModel != null ? deployableModel.getName() : null;
	}
	
	public DeployableProxy getParent() {
		return intSvrProxy;
	}
	
	public Enumeration<MessageFlowProxy> getMessageFlows(Properties properties) {
		populateMsgflowProxies();
		return Collections.enumeration(msgflowProxies);
	}

	private void populateMsgflowProxies() {
		if (msgflowProxies.isEmpty()) {
			List<MsgflowModel> msgflowModels = deployableModel.getMsgflowModels();
			for (MsgflowModel msgflowModel : msgflowModels) {
				MessageFlowProxy proxy = new MessageFlowProxy(this, msgflowModel);
				msgflowProxies.add(proxy);
			}
		}
	}
	
	public Enumeration<SubFlowProxy> getSubFlows(Properties properties) { //throws IntegrationAdminException {
		populateSubflowProxies();
		return Collections.enumeration(subflowProxies);
	}

	private void populateSubflowProxies() {
		if (subflowProxies.isEmpty()) {
			List<SubflowModel> subflowModels = deployableModel.getSubflowModels();
			for (SubflowModel subflowModel : subflowModels) {
				SubFlowProxy proxy = new SubFlowProxy(this, subflowModel);
				subflowProxies.add(proxy);
			}
		}
	}
	
	public Enumeration<StaticLibraryProxy> getStaticLibraries(Properties properties) { //throws IntegrationAdminException {
		populateStaticLibraryProxies();
		return Collections.enumeration(staticLibProxies);
	}

	private void populateStaticLibraryProxies() {
		if (staticLibProxies.isEmpty()) {
			List<StaticLibModel> staticLibModels = deployableModel.getStaticLibraries();
			for (StaticLibModel slModel : staticLibModels) {
				StaticLibraryProxy proxy = new StaticLibraryProxy(intSvrProxy, slModel);
				staticLibProxies.add(proxy);
			}
		}
	}
	
	public ExecutionGroupProxy getExecutionGroup() {
		return (ExecutionGroupProxy)intSvrProxy;
	}
	
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		return true; // FIXME
	}

	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "ApplicationProxy [adminObjectProxy=" + getName() + ", executionGroupProxy=" + getName() + "]"; // FIXME parent.getName() 
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}
}
