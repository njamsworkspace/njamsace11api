package com.w3p.api.config.model;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.ibm.integration.admin.http.HttpResponse;
import com.w3p.api.config.proxy.ConfigManagerProxyLoggedException;


public class SubflowModel extends FlowModel {
	private static final Logger logger = LoggerFactory.getLogger(SubflowModel.class);

	private final List<FlowNodeModel> flowNodeModels = new LinkedList<>(); // all Nodes in the flow
	private final Map<String, FlowNodeModel> flowNodeModelsMap = new LinkedHashMap<>();
	
	private final List<FlowNodeConnectionModel> flowNodeConnectionModels = new LinkedList<>(); // All connections in the flow 
	private final Map<String, FlowNodeConnectionModel> flowNodeConnectionModelsMap = new LinkedHashMap<>(); // k = sourceNode.name 

	
	public SubflowModel(DeployableModel parentModel) { //AbstractModel parentModel) {
		super(parentModel);
	}
	
	public SubflowModel create(JsonNode subflowJsonNode) {
		super.flowJsonNode = subflowJsonNode;		
		return this;
	}
	
	protected void populate() {
		// Each item in the 'links' array, refers to the message or sub flow where it is used.
		// This appears to be the only way to get the nodes for the subflow
		// We only want to use the first item
		JsonNode subflowNodeLinksArray = super.flowJsonNode.path("links");
		if (!subflowNodeLinksArray.isEmpty()) {
			JsonNode subflowNodeLink = subflowNodeLinksArray.get(0);
			String usingParentUri = subflowNodeLink.path("uri").asText();		
			try {		
				// Call the HttpClient for this uri appended with 'depth=3'
				HttpResponse response = parentModel.httpClient. getMethod(usingParentUri+"?depth=3", true);
				if (response.getStatusCode() == 200) {
					String respBody = response.getBody();
					createFlowNodeModels(respBody);
				} else {
					String msg = String.format("HTTP error '%d' when getting the nodes for subflow '%s' in parent '%s'", 
							response.getStatusCode(), name, parentModel.getName());
					logger.error(msg);
					throw new ConfigManagerProxyLoggedException(msg);
				} 
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		} else {
			logger.warn(this.getName()+" does not have any connections between nodes" );
		}

		if (!flowNodeModels.isEmpty()) {
			// Loop round the FlowNodeModels to create the FlowNodeConnections for each node
			for (FlowNodeModel flowNodeModel : flowNodeModels) {
				JsonNode connectionsNode = flowNodeModel.getConnectionsNode();
				for (JsonNode connection : connectionsNode) {
					// Determine whether current connection is incoming or outgoing
					if (connection.path("to").asText().isEmpty()) {
						// This is an incoming connection.
						// Handle changes to subflow Input and Output nodes
						String sourceNodeName = null;
						String nodeName = connection.path(NAME).asText();
						if (nodeName.contains("#InTerminal")) {
							sourceNodeName = StringUtils.substringAfter(nodeName, "#InTerminal.");
						} else if (nodeName.contains("#OutTerminal")) {
							sourceNodeName = StringUtils.substringAfter(nodeName, "#OutTerminal.");
						} else {
							sourceNodeName = nodeName;
						}
						FlowNodeModel sourceNode = flowNodeModelsMap.get(sourceNodeName);
						String sourceTerminal = connection.path("sourceTerminal").asText();
						String targetTerminal = connection.path("targetTerminal").asText();
						FlowNodeConnectionModel fncm = new FlowNodeConnectionModel(sourceNode, sourceTerminal, flowNodeModel, targetTerminal);
						flowNodeConnectionModels.add(fncm);
					} else {
						// This is an outgoing connection - do not create as it will be a duplicate of an incoming connection!					
					}	
				}
			}
		} else {
			logger.warn(this.getName()+" does not have any nodes" );
		}	
	}

	private void createFlowNodeModels(String respBody) throws JsonProcessingException, JsonMappingException {
		JsonNode usingParent = mapper.readTree(respBody);
		JsonNode subflowNodesArray = usingParent.path(CHILDREN).path("nodes").path(CHILDREN);
		for (JsonNode subflowNode : subflowNodesArray) {
			FlowNodeModel flowNodeModel = new FlowNodeModel(this).create(subflowNode).populate();
			flowNodeModels.add(flowNodeModel);
			flowNodeModelsMap.put(flowNodeModel.getName(), flowNodeModel);
		}
	}
	

	public List<FlowNodeModel> getFlowNodeModels() {
		if (flowNodeModels.isEmpty()) {
			populate();
		}
		return flowNodeModels;
	}
	
	public FlowNodeModel getFlowNodeByName(String name) {
		getFlowNodeModels(); // Ensure this has values.
		if (flowNodeModelsMap.isEmpty()) {
			flowNodeModels.forEach(fnm -> {
				flowNodeModelsMap.put(fnm.getName(), fnm);
			});
		}
		return flowNodeModelsMap.get(name);
	}

	public List<FlowNodeConnectionModel> getFlowNodeConnectionModels() {
		if (flowNodeConnectionModels.isEmpty()) {
			populate(); // This populates flowNodes and flowNodeConnections
		}
		return flowNodeConnectionModels;
	}
	
	public FlowNodeConnectionModel getFlowNodeConnectionModelByName(String sourceNodeName) {
		getFlowNodeConnectionModels(); // Ensure this has values.
		if (flowNodeConnectionModelsMap.isEmpty()) {
			flowNodeConnectionModels.forEach(fncm -> {
				flowNodeConnectionModelsMap.put(fncm.getSourceNodeName(), fncm);
			});
		}
		return flowNodeConnectionModelsMap.get(sourceNodeName);
	}
	
	public String getRuntimeProperty(String name) throws ConfigManagerProxyLoggedException, IllegalArgumentException { //FIXME create them
		super.flowJsonNode = refresh();
		String propName = StringUtils.substringAfter(name, "/");
		String propValue = super.flowJsonNode.path("active").path(propName).asText();
		return propValue.isEmpty() ? null : propValue; // Caller tests for null as 'not set'
	}
	
	private JsonNode refresh() throws ConfigManagerProxyLoggedException {		
		try {
			HttpResponse response = httpClient.getMethod(super.flowJsonNode.path(URI).asText());
			if (response.getStatusCode() != 200) {
				String error = response.getError();
				String reason = response.getReason();
				String cause = String.format("Error when refreshing Message Flow '%s' in Application '%s'. Http status code: '%d'", getName(), parentModel.getName(), response.getStatusCode());
				logger.error(String.format("Cause: %s.\n  Reason: %s.\n  Error: %s.", cause, reason, error));
				throw new ConfigManagerProxyLoggedException(cause);
			}
			return mapper.readTree(response.getBody());
		} catch (IOException | InterruptedException e) {
			throw new ConfigManagerProxyLoggedException(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	@Override
	public String toString() {
		return "SubflowModel [getName()=" + getName() + "]";
	}
}
