package com.w3p.api.config.model;

//import static com.w3p.api.config.proxy.AdministeredObject.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.ibm.integration.admin.http.HttpClient;
import com.ibm.integration.admin.http.HttpResponse;
import com.ibm.integration.admin.proxy.IntegrationAdminException;

public class IntNodeModel extends AbstractModel {
	
	private JsonNode intNodeNode;
	
	private final List<IntSvrModel> intSvrModels = new LinkedList<>();
	
	
	public IntNodeModel() {
	}	
	
	public IntNodeModel connect(String integrationNodeHost, int integrationNodePort, String username, String password, boolean useSSL) {
		super.integrationNodeHost = integrationNodeHost;
		try {
			httpClient = new HttpClient(integrationNodeHost, integrationNodePort, username, password, useSSL);
			
			HttpResponse response = httpClient.getMethod(API_ROOT+"?depth=2");
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error connecting to Integration Node using hostname '%s' and port '%s', Status '%d', Message '%s'.",
					integrationNodeHost, integrationNodePort, response.getStatusCode(), response.getError() ));
			}
						
			intNodeNode = mapper.readTree(response.getBody());
			super.restAdminListenerPort = intNodeNode.path(PROPERTIES).path("restAdminListenerPort").asInt(); 
		} catch (IOException | InterruptedException e) {
			throw new RuntimeException(String.format("Error creating the Integration Node using hostname '%s' and port '%s'. Exception msg: \n'%s'",
				integrationNodeHost, integrationNodePort, e.getMessage()), e);
		}
		
		return this;
	}
	
	private IntNodeModel populate() throws IntegrationAdminException {
		JsonNode intSvrsArrayNode = intNodeNode.path(CHILDREN).path(SERVERS).path(CHILDREN);
		for (JsonNode intSvrLinkNode : intSvrsArrayNode) {
				try {
					IntSvrModel intSvrModel = new IntSvrModel(httpClient, intSvrLinkNode);
					if (intSvrModel.isRunning()) {
						intSvrModel.create();
						intSvrModels.add(intSvrModel);
					}
				} catch (Exception e) { //IOException | InterruptedException e) {
					e.printStackTrace(); // FIXME TEMP
					throw new IntegrationAdminException(e, "Getting Int Servers for Int Node");
				}
		}
		
		return this;
	}	
	
	public String getName() {
		if (name == null) {
			name = intNodeNode != null ? intNodeNode.path(NAME).asText() : null;
		}
		return name;
	}

	public List<IntSvrModel> getIntSvrModels() throws IntegrationAdminException {
		if (intSvrModels.isEmpty()) {
			populate();
		}
		return intSvrModels;
	}

}
