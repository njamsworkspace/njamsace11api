package com.w3p.api.config.model;

import com.fasterxml.jackson.databind.JsonNode;

public class SharedLibModel extends DeployableModel {
	

	public SharedLibModel(DeployableModel intSvrModel, JsonNode shLibNode) {
		super(intSvrModel, shLibNode);
	}
	
	public SharedLibModel create() {
		return (SharedLibModel) super.create();
	}
	
	@Override
	public String toString() {
		return "SharedLibModel [getName()=" + getName() + "]";
	}	
}
