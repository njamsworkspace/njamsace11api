package com.w3p.api.config.proxy;

import com.w3p.api.config.model.RestApiModel;


public class RestApiProxy extends DeployableProxy {
	

	public RestApiProxy(DeployableProxy intSvrProxy, RestApiModel apiModel) {
		super(intSvrProxy, apiModel);
	}

	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "RestApiProxy [adminObjectProxy=" + getName() + ", executionGroupProxy=" + getName() + "]"; // FIXME parent.getName() 
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}	
}
