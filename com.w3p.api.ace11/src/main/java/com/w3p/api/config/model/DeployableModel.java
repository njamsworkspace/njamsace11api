package com.w3p.api.config.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.ibm.integration.admin.http.HttpClient;
import com.ibm.integration.admin.http.HttpResponse;

public abstract class DeployableModel extends AbstractModel {
	
	protected DeployableModel parentModel;
	private JsonNode deployableDetailsNode;
	
	private JsonNode deployableNode;
	private JsonNode deployableNodeChildren; // subFlows, libraries, messageFlows 

	protected final List<MsgflowModel> msgflowModels = new LinkedList<>();
	protected final List<SubflowModel> subflowModels = new LinkedList<>();
	protected final List<StaticLibModel> staticLibModels = new LinkedList<>();

	
	
	public DeployableModel() {
		super();
	}

	public DeployableModel(HttpClient httpClient) {
		super.httpClient = httpClient;
	}

	public DeployableModel(DeployableModel parentModel, JsonNode deployableNode) {
		super.httpClient = parentModel.getHttpClient();
		this.parentModel = parentModel;
		this.deployableNode = deployableNode;
	}
	
	protected DeployableModel create() {
		String deployableLinkUri = deployableNode.path(URI).asText();
		try {
			HttpResponse response = httpClient.getMethod(deployableLinkUri);
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error getting details with children for App/RestApi/Service with URI '%s'. Status '%d', Message '%s'.",
					deployableLinkUri, response.getStatusCode(), response.getError() ));
			}
			deployableDetailsNode = mapper.readTree(response.getBody());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}	
		
		return this;
	}
	
	public String getName() {
		if (name == null) {
			name = deployableNode.path(NAME).asText(); 
		}
		return name;
	}
	
	public boolean isRunning() {
		return deployableDetailsNode.path("active").path("isRunning").asBoolean();
	}

	public List<MsgflowModel> getMsgflowModels() {
		if (msgflowModels.isEmpty()) {
			populateMsgFlows();
		}
		return msgflowModels;
	}

	private void populateMsgFlows() {
		try {
			populate();
			JsonNode msgflowNodes = deployableNodeChildren.path("messageFlows").path(CHILDREN); // JsonArrayNode
			for (JsonNode msgflowNode : msgflowNodes) {
				MsgflowModel msgflowModel = new MsgflowModel(this).create(msgflowNode);
				msgflowModels.add(msgflowModel);
			}
		} catch (IOException | InterruptedException e) { // | IntegrationAdminException e) {
			e.printStackTrace();
		}
	}
	
	public List<SubflowModel> getSubflowModels() {
		if (subflowModels.isEmpty()) {
			populateSubFlows();
		}
		return subflowModels;
	}
	
	private void populateSubFlows() {
		try {
			populate();
			JsonNode subflowNodes = deployableNodeChildren.path("subFlows").path(CHILDREN); // JsonArrayNode
			for (JsonNode subflowNode : subflowNodes) {
				SubflowModel subflowModel = new SubflowModel(this).create(subflowNode);
				subflowModels.add(subflowModel);
			}
		} catch (IOException | InterruptedException e) { // | IntegrationAdminException e) {
			e.printStackTrace();
		}
	}

	public List<StaticLibModel> getStaticLibraries() {
		if (staticLibModels.isEmpty()) {
			populateStaticLibraries();
		}
		return staticLibModels;
	}

	private void populateStaticLibraries() {
		try {
			populate();
			JsonNode staticLibNodes = deployableNodeChildren.path("libraries").path(CHILDREN); // JsonArrayNode
			for (JsonNode staticLibNode : staticLibNodes) {
				StaticLibModel staticLibModel = new StaticLibModel(parentModel, staticLibNode).create(); // FIXME is it correct to use the IntSvr parent here? It might be OK if parent not involved.
				staticLibModels.add(staticLibModel);
			}
		} catch (IOException | InterruptedException e) { // | IntegrationAdminException e) {
			e.printStackTrace();
		}
	}
	
	protected void populate() throws ConnectException, SocketTimeoutException, FileNotFoundException, UnsupportedEncodingException, MalformedURLException, IOException, InterruptedException {
		if (deployableNodeChildren == null) {
			String appUri = deployableNode.path(URI).asText();
			// This gets the App's details and all of its children 
			HttpResponse response = httpClient.getMethod(String.format("%s?depth=5", appUri));
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error gettng all children for Application '%s', Status '%d', Message '%s'.",
						getName(), response.getStatusCode(), response.getError() ));
			}
			deployableNodeChildren = mapper.readTree(response.getBody()).path(CHILDREN); // subFlows, libraries, messageFlows
		}		
	}
}
