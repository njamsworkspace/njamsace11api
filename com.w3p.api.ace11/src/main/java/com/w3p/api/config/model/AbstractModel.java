package com.w3p.api.config.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.integration.admin.http.HttpClient;

public abstract class AbstractModel {
	
	protected static final String SERVERS = "servers";
	protected static final String CHILDREN = "children";
	protected static final String NAME = "name";
	protected static final String PROPERTIES = "properties";
	protected static final String URI = "uri";
	protected static final String API_ROOT = "/apiv2";
	
	protected HttpClient httpClient;
	
	protected int restAdminListenerPort;
	protected String integrationNodeHost;
	
	protected ObjectMapper mapper = new ObjectMapper();
	protected String name;
	protected String uri;

	public AbstractModel() {
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}

	public String getName() {
		return name;
	}

	public String getUri() {
		return uri;
	}
}
