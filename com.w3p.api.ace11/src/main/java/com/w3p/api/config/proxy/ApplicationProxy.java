package com.w3p.api.config.proxy;

import com.w3p.api.config.model.AppModel;


public class ApplicationProxy extends DeployableProxy {

	public ApplicationProxy(DeployableProxy intSvrProxy, AppModel appModel) {
		super(intSvrProxy, appModel);
	}


	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "ApplicationProxy [adminObjectProxy=" + getName() + ", executionGroupProxy=" + getName() + "]"; 
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}
}
