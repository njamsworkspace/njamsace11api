package com.w3p.api.config.proxy;

import java.awt.Point;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.w3p.api.config.model.FlowNodeConnectionModel;
import com.w3p.api.config.model.FlowNodeModel;
import com.w3p.api.config.model.MsgflowModel;


public class MessageFlowProxy extends FlowProxy {
	
	private final MsgflowModel msgflowModel;
			
	private List<MessageFlowProxy.Node> msgflowNodes = new LinkedList<>();
	private List<MessageFlowProxy.NodeConnection> msgflowConnections = new LinkedList<>();
	// Actions
	private Map<String, String> actionsUnavailable; // K:command, V:uri POST e.g "start-monitoring" 
	private Map<String, String> actionsAvailable;   // K:command, V:uri POST e.g "stop-monitoring"
	
	
	public MessageFlowProxy(AdministeredObject appProxy, MsgflowModel msgflowModel) {
		super(appProxy);
		this.msgflowModel = msgflowModel;
	}
	
	protected void populate() {
		List<FlowNodeModel> nodeModels = msgflowModel.getFlowNodeModels();
		for (FlowNodeModel flowNodeModel : nodeModels) {
			Node node = new Node(flowNodeModel);
			msgflowNodes.add(node);
		}
		
		List<FlowNodeConnectionModel> connectionModels = msgflowModel.getFlowNodeConnectionModels();
		for (FlowNodeConnectionModel flowNodeConnectionModel : connectionModels) {
			NodeConnection nodeConnection = new NodeConnection(flowNodeConnectionModel);
			msgflowConnections.add(nodeConnection);
		}	
	}


	@Override
	public String getName() {
		return msgflowModel.getName();
	}
	
	public void setRuntimeProperty(String propertyName, String propertyValue) throws ConfigManagerProxyLoggedException, IllegalArgumentException {
		msgflowModel.setRuntimeProperty(propertyName, propertyValue);
	}
	
	public String getRuntimeProperty(String name) throws ConfigManagerProxyLoggedException, IllegalArgumentException {
		return msgflowModel.getRuntimeProperty(name);
	}

	public Enumeration<Node> getNodes() {
		if (msgflowNodes.isEmpty()) {
			populate();
		}
		return Collections.enumeration(msgflowNodes);
	}
	public Enumeration<NodeConnection> getNodeConnections() {
		if (msgflowConnections.isEmpty()) {
			populate();
		}
		return Collections.enumeration(msgflowConnections);
	}
	
	public Node getNewNode(FlowNodeModel model) {
		Node n = new Node(model);
		return n;
	}
	
	public NodeConnection getNewNodeConnection(FlowNodeConnectionModel model) {
		NodeConnection nc = new NodeConnection(model);
		return nc;
	}
	
	@Override
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		return msgflowModel.isRunning();
	}

	@Override
	public String toString() {
		return "MessageFlowProxy [msgflowConnections=" + msgflowConnections + ", actionsUnavailable="
				+ actionsUnavailable + ", actionsAvailable=" + actionsAvailable + ", getName()=" + getName() + "]";
	}

	/*
	 * Inner Classes: Node and NodeConnection
	 */
	public class Node {
		private final FlowNodeModel model;
		public Node(FlowNodeModel model) {
			this.model = model;
		}		
		public String getName() {
			return model.getName();
		}
		public String getType() {
			return model.getType();
		}
		public Point getLocation() {
			return model.getLocation();
		}
		public Properties getProperties() {
			return model.getProperties();
		}
		@Override
		public String toString() {
			return "Node [model=" + model + "]";
		}
	}

	public class NodeConnection {
		private final FlowNodeConnectionModel model;		
		public NodeConnection(FlowNodeConnectionModel model) {
			this.model = model;
		}		
		public FlowNodeModel getSourceNode() {
			return model.getSourceNode();
		}
		public String getSourceOutputTerminal() {
			return  model.getSourceOuputTerminal();
		}
		public FlowNodeModel getTargetNode() {
			return model.getTargetNode();
		}
		public String getTargetInputTerminal() {
			return model.getTargetInputTerminal();
		}
		@Override
		public String toString() {
			return "NodeConnection [getSourceNode()=" + getSourceNode() + ", getTargetNode()=" + getTargetNode() + "]";
		}	
	}
}
