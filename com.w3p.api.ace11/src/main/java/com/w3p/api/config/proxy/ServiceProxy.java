package com.w3p.api.config.proxy;

import com.w3p.api.config.model.ServiceModel;


public class ServiceProxy extends DeployableProxy {


	public ServiceProxy(DeployableProxy intSvrProxy, ServiceModel serviceModel) {
		super(intSvrProxy, serviceModel);
	}

	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "ServiceProxy [adminObjectProxy=" + getName() + ", executionGroupProxy=" + getName() + "]"; // FIXME parent.getName() 
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}
	
}
