package com.w3p.api.config.model;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.ibm.integration.admin.http.HttpResponse;
import com.w3p.api.config.proxy.ConfigManagerProxyLoggedException;


public class MsgflowModel extends FlowModel {
	private static final Logger logger = LoggerFactory.getLogger(MsgflowModel.class);

	private final List<FlowNodeModel> flowNodeModels = new LinkedList<>(); // all Nodes in the flow
	private final Map<String, FlowNodeModel> flowNodeModelsMap = new LinkedHashMap<>();
	
	private final List<FlowNodeConnectionModel> flowNodeConnectionModels = new LinkedList<>(); // All connections in the flow 
	private final Map<String, FlowNodeConnectionModel> flowNodeConnectionModelsMap = new LinkedHashMap<>(); // k = sourceNode.name 

	
	public MsgflowModel(DeployableModel parentModel) { // AbstractModel parentModel) {
		super(parentModel);
	}
	
	public MsgflowModel create(JsonNode msgflowJsonNode) {
		super.flowJsonNode = msgflowJsonNode;		
		return this;
	}
	
	protected void populate() {
		JsonNode msgflowNodesArray = super.flowJsonNode.path(CHILDREN).path("nodes").path(CHILDREN);
		for (JsonNode msgflowNodeNode : msgflowNodesArray) {
			FlowNodeModel flowNodeModel = new FlowNodeModel(this).create(msgflowNodeNode).populate();
			flowNodeModels.add(flowNodeModel);
			flowNodeModelsMap.put(flowNodeModel.getName(), flowNodeModel);
		}		
		// Loop round the FlowNodeModels to create the FlowNodeConnections for each node
		for (FlowNodeModel flowNodeModel : flowNodeModels) {
			JsonNode connectionsNode = flowNodeModel.getConnectionsNode();
			for (JsonNode connection : connectionsNode) {
				// Determine whether current connection is incoming or outgoing
				if (connection.path("to").asText().isEmpty()) {
					// This is an incoming connection.
					FlowNodeModel sourceNode = flowNodeModelsMap.get(connection.path(NAME).asText());
					String sourceTerminal = connection.path("sourceTerminal").asText();
					String targetTerminal = connection.path("targetTerminal").asText();
					FlowNodeConnectionModel fncm = new FlowNodeConnectionModel(sourceNode, sourceTerminal, flowNodeModel, targetTerminal);
					flowNodeConnectionModels.add(fncm);
				}else {
					// This is an outgoing connection - do not create as it will be a duplicate of an incoming connection!					
				}	 				
			}
		}
	}
	
	public List<FlowNodeModel> getFlowNodeModels() {
		if (flowNodeModels.isEmpty()) {
			populate();
		}
		return flowNodeModels;
	}
	
	public FlowNodeModel getFlowNodeByName(String name) {
		getFlowNodeModels(); // Ensure this has values.
		if (flowNodeModelsMap.isEmpty()) {
			flowNodeModels.forEach(fnm -> {
				flowNodeModelsMap.put(fnm.getName(), fnm);
			});
		}
		return flowNodeModelsMap.get(name);
	}

	public List<FlowNodeConnectionModel> getFlowNodeConnectionModels() {
		if (flowNodeConnectionModels.isEmpty()) {
			populate(); // This populates flowNodes and flowNodeConnections
		}
		return flowNodeConnectionModels;
	}
	
	public FlowNodeConnectionModel getFlowNodeConnectionModelByName(String sourceNodeName) {
		getFlowNodeConnectionModels(); // Ensure this has values.
		if (flowNodeConnectionModelsMap.isEmpty()) {
			flowNodeConnectionModels.forEach(fncm -> {
				flowNodeConnectionModelsMap.put(fncm.getSourceNodeName(), fncm);
			});
		}
		return flowNodeConnectionModelsMap.get(sourceNodeName);
	}
	
	public String getMonitoringProfile() {
		return super.flowJsonNode.path("active").path("monitoringProfile").asText();
	}
	
	public String getRuntimeProperty(String name) throws ConfigManagerProxyLoggedException, IllegalArgumentException { //FIXME create them
		super.flowJsonNode = refresh();
		String propName = StringUtils.substringAfter(name, "/");
		String propValue = super.flowJsonNode.path("active").path(propName).asText();
		return propValue.isEmpty() ? null : propValue; // Caller tests for null as 'not set'
	}

	public void setRuntimeProperty(String name, String value) throws ConfigManagerProxyLoggedException, IllegalArgumentException {
		if (name.equals("This/monitoring")) {
			if (value.equals("active")) {
				startMonitoring();
			} else if (value.equals("inactive")) {
				stopMonitoring();
			} else {
				throw new IllegalArgumentException(String.format("Invalid runtime property value '%s'.", value));
			}
		} else if (name.equals("This/monitoringProfile")) {
			if (value.isEmpty()) {
				detachMonitoringProfile();
			} else {
				attachMonitoringProfile(value);
			}
		} else {
			throw new IllegalArgumentException(String.format("Invalid runtime property name '%s'.", name));
		}
	}
	
	private JsonNode refresh() throws ConfigManagerProxyLoggedException {		
		try {
			HttpResponse response = httpClient.getMethod(super.flowJsonNode.path(URI).asText());
			if (response.getStatusCode() != 200) {
				String error = response.getError();
				String reason = response.getReason();
				String cause = String.format("Error when refreshing Message Flow '%s' in Application '%s'. Http status code: '%d'", getName(), parentModel.getName(), response.getStatusCode());
				logger.error(String.format("Cause: %s.\n  Reason: %s.\n  Error: %s.", cause, reason, error));
				throw new ConfigManagerProxyLoggedException(cause);
			}
			return mapper.readTree(response.getBody());
		} catch (IOException | InterruptedException e) {
			throw new ConfigManagerProxyLoggedException(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	private void startMonitoring() throws ConfigManagerProxyLoggedException {
		// Check value of monitoring
		super.flowJsonNode = refresh();
		//http://localhost:4418/apiv2/servers/njamsDemo/applications/parentModel.getName()/messageflows/getName()/stop-monitoring
		String startPath = super.flowJsonNode.path("actions").path("available").path("start-monitoring").asText();		
		if (startPath.isEmpty()) {
			return; // monitoring is already active
		}

		try {
			HttpResponse response = httpClient.postMethod(startPath, "");
			if (response.getStatusCode() != 200) {
				String error = response.getError();
				String reason = response.getReason();
				String cause = String.format("Error when starting monitoring for Message Flow '%s' in Application '%s'. Http status code: '%d'", getName(), parentModel.getName(), response.getStatusCode());
				logger.error(String.format("Cause: %s.\n  Reason: %s.\n  Error: %s.", cause, reason, error));
				throw new ConfigManagerProxyLoggedException(cause);
			}
		} catch (IOException | InterruptedException e) {
			throw new ConfigManagerProxyLoggedException(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	private void stopMonitoring() throws ConfigManagerProxyLoggedException {
		// Check value of monitoring
		super.flowJsonNode = refresh();
		//http://localhost:/apiv2/servers/njamsDemo/applications/parentModel.getName()/messageflows/getName()/stop-monitoring
		String stopPath = super.flowJsonNode.path("actions").path("available").path("stop-monitoring").asText();
		if (stopPath.isEmpty()) {
			return; // monitoring isn't active
		}

		try {
			HttpResponse response = httpClient.postMethod(stopPath, "");
			if (response.getStatusCode() != 200) {
				String error = response.getError();
				String reason = response.getReason();
				String cause = String.format("Error when stopping monitoring for Message Flow '%s' in Application '%s'. Http status code: '%d'", getName(), parentModel.getName(), response.getStatusCode());
				logger.error(String.format("Cause: %s.\n  Reason: %s.\n  Error: %s.", cause, reason, error));
				throw new ConfigManagerProxyLoggedException(cause);
			}
		} catch (IOException | InterruptedException e) {
			throw new ConfigManagerProxyLoggedException(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	private void attachMonitoringProfile(String monProfileName) throws ConfigManagerProxyLoggedException {
		logger.debug("*** Attaching  Mon Profile: "+monProfileName);

		// Check value of monitoring profile
		super.flowJsonNode = refresh();
		String attachPath = super.flowJsonNode.path("actions").path("available").path("attach-monitoring-profile").asText();
		if (attachPath.isEmpty()) {
			return; // monitoring profile is already attached
		}

		// /apiv2/servers/njamsDemo/applications/DemoApp/messageflows/DemoFlow/attach-monitoring-profile?profile=DemoFlow&project=DemoAppPolicy
		String policyProject = String.format("%sPolicy", parentModel.name); // Policy-name convention is: '<app-name>Policy' or api-name or other parent
		String policyProjectUri = attachPath.concat("?profile=").concat(getName()).concat("&project=").concat(policyProject) ; // Policy-name convention is: '<app-name>Policy' or api-name or other parent
		
		try {
			HttpResponse response = super.httpClient.postMethod(policyProjectUri, "", false); // 'true' = encodePath
			if (response.getStatusCode() != 200) {
				String message = response.getError();
				String cause = String.format("Error when attaching monitoring profile '%s' in policy project '%s' for Message Flow '%s' in Application '%s'. Http status code: '%d'",
						getName(), policyProject, getName(), parentModel.getName(), response.getStatusCode());
				logger.error(String.format("\n Cause:\n  %s.", cause, message));
				throw new ConfigManagerProxyLoggedException(cause);
			} 
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	private void detachMonitoringProfile() throws ConfigManagerProxyLoggedException {
		// Check value of monitoring profile
		super.flowJsonNode = refresh();
		// /apiv2/servers/njamsDemo/applications/DemoApp/messageflows/DemoFlow/detach-monitoring-profile
		String detachPath = super.flowJsonNode.path("actions").path("available").path("detach-monitoring-profile").asText();
		if (detachPath.isEmpty()) {
			return; // monitoring profile isn't attached
		}

		try {
			HttpResponse response = httpClient.postMethod(detachPath, "");
			if (response.getStatusCode() != 200) {
				String error = response.getError();
				String reason = response.getReason();
				String cause = String.format("Error when detaching monitoring profile '%s' from Message Flow '%s' in Application '%s'. Http status code: '%d'",
						getName(), getName(), parentModel.getName(), response.getStatusCode());
				logger.error(String.format("Cause: %s.\n  Reason: %s.\n  Error: %s.", cause, reason, error));
				throw new ConfigManagerProxyLoggedException(cause);
			}
		} catch (IOException | InterruptedException e) {
			throw new ConfigManagerProxyLoggedException(e.getMessage(), e);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	public String toString() {
		return "MsgflowModel [getName()=" + getName() + "]";
	}


}
