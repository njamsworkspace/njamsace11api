package com.w3p.api.config.proxy;

public class ConfigManagerProxyPropertyNotInitializedException extends Exception {

	public ConfigManagerProxyPropertyNotInitializedException() {
		// TODO Auto-generated constructor stub
	}

	public ConfigManagerProxyPropertyNotInitializedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConfigManagerProxyPropertyNotInitializedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ConfigManagerProxyPropertyNotInitializedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConfigManagerProxyPropertyNotInitializedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
