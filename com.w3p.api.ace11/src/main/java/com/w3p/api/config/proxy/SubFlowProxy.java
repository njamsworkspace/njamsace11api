package com.w3p.api.config.proxy;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import com.w3p.api.config.model.FlowNodeConnectionModel;
import com.w3p.api.config.model.FlowNodeModel;
import com.w3p.api.config.model.SubflowModel;
import com.w3p.api.config.proxy.MessageFlowProxy.Node;
import com.w3p.api.config.proxy.MessageFlowProxy.NodeConnection;


public class SubFlowProxy extends FlowProxy {
	
	private final SubflowModel subflowModel; 
	
	private List<MessageFlowProxy.Node> subflowNodes = new LinkedList<>();
	private List<MessageFlowProxy.NodeConnection> subflowConnections = new LinkedList<>();
	
	
	public SubFlowProxy(AdministeredObject adminObjectProxy, SubflowModel subflowModel) {
		super(adminObjectProxy);
		this.subflowModel = subflowModel;
	}
	
	@Override
	public String getName() {
		return subflowModel.getName();
	}

	@Override
	protected void populate() {
		List<FlowNodeModel> nodeModels = subflowModel.getFlowNodeModels();
		for (FlowNodeModel flowNodeModel : nodeModels) {
			MessageFlowProxy.Node node = new MessageFlowProxy(null, null).getNewNode(flowNodeModel);
			subflowNodes.add(node);
		}
		
		List<FlowNodeConnectionModel> connectionModels = subflowModel.getFlowNodeConnectionModels();
		for (FlowNodeConnectionModel flowNodeConnectionModel : connectionModels) {
			MessageFlowProxy.NodeConnection nodeConnection = new MessageFlowProxy(null, null).getNewNodeConnection(flowNodeConnectionModel);
			subflowConnections.add(nodeConnection);
		}	
		
	}
	
	public Enumeration<Node> getNodes() {
		if (subflowNodes.isEmpty()) {
			populate();
		}
		return Collections.enumeration(subflowNodes);
	}
	public Enumeration<NodeConnection> getNodeConnections() {
		if (subflowConnections.isEmpty()) {
			populate();
		}
		return Collections.enumeration(subflowConnections);
	}

	@Override
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		return subflowModel.isRunning();
	}

	@Override
	public String toString() {
		return "SubFlowProxy [getName()=" + getName() + "]";
	}
}
