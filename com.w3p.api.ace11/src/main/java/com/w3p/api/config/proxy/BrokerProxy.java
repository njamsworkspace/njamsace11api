package com.w3p.api.config.proxy;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.ibm.integration.admin.proxy.IntegrationAdminException;
import com.w3p.api.config.model.IntNodeModel;
import com.w3p.api.config.model.IntSvrModel;
import com.w3p.api.config.model.PolicyModel;


public class BrokerProxy extends AdministeredObject {

	private static BrokerProxy INSTANCE;
	protected static IntNodeModel intNodeModel;
	protected static BrokerConnectionParameters bcp;
	
	private final Map<String, ExecutionGroupProxy> intSvrsMap = new LinkedHashMap<>();
	private final List<ExecutionGroupProxy> intSvrs = new LinkedList<>();
	
	private final Map<String, ConfigurableService> configServices = new HashMap<>();
	
	private int timeToWaitMs; // Timeout for remote call
	
	private BrokerProxy() {
	}

	public static BrokerProxy getInstance(BrokerConnectionParameters bcp) {
		if(INSTANCE == null) {
            INSTANCE = new BrokerProxy();
        }
		BrokerProxy.bcp = bcp;
		connect(bcp);
		return INSTANCE;
	}
	
	/**
	 * Connects, using the parameters to the web administration port
	 * on which an integration node is listening on the named host.
	 * @param integrationNodeHost The host name of the integration nod 
	 * @param integrationNodePort The web administration port number of the Int node
	 * @param username (default is "")
	 * @param password (default is "")
	 * @param useSSL  (default is 'false')
	 * @return BrokerProxy connected instance.
	 * @throws RuntimeException If the connection could not be established - message 'Unable to connect to the int node' 
	 */
	private static void connect(BrokerConnectionParameters bcp) {
		try {			
			intNodeModel = new IntNodeModel().connect(bcp.getIntegrationNodeHost(), bcp.getIntegrationNodePort(), bcp.getUsername(), bcp.getPassword(), bcp.isUseSSL());
		} catch (Exception e) {
			throw(e); // FIXME later
		}
	}
	
	/**
	 * @return Int Node's name
	 */
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		return intNodeModel.getName();
	}
	
	public static BrokerConnectionParameters getBrokerConnectionParameters() {
		return bcp;
	}

	public ExecutionGroupProxy getExecutionGroupByName(String name) throws IntegrationAdminException {
		if (intSvrsMap.isEmpty()) {
			getAllExecutionGroups();
			intSvrs.forEach((is) -> {
				try {
					intSvrsMap.put(is.getName(), is);
				} catch (ConfigManagerProxyPropertyNotInitializedException e) {
					e.printStackTrace();
				}
			});
		}

		return intSvrsMap.get(name);
	}

	private List<ExecutionGroupProxy> getAllExecutionGroups() throws IntegrationAdminException {
		populate();
		return intSvrs;
	}
	
	public void createConfigurableService(String serviceType, String serviceName) throws ConfigManagerProxyLoggedException, ConfigManagerProxyPropertyNotInitializedException {
		PolicyModel policyModel = new PolicyModel(serviceType, serviceName);
		ConfigurableService cs = new ConfigurableService(this, policyModel);
		configServices.put(serviceName, cs);
	}
	
	public void deleteConfigurableService(String serviceType, String serviceName) throws ConfigManagerProxyLoggedException, ConfigManagerProxyPropertyNotInitializedException {
//		ConfigurableService cs = 
		getConfigurableService(serviceType, serviceName);
		configServices.remove(serviceName);
	}
	
	public ConfigurableService getConfigurableService(String serviceType, String serviceName) throws ConfigManagerProxyPropertyNotInitializedException, ConfigManagerProxyLoggedException {
		final ConfigurableService[] selected = new ConfigurableService[1];
		configServices.forEach((k,v) -> {
			if (k.equals(serviceName) && v.getPolicyModel().getServiceType().equals(serviceType)) {
				selected[0] = v;
			}
		});
		
		return selected[0];
	}
	

	public void setSynchronous(int timeToWaitMs) {
		this.timeToWaitMs = timeToWaitMs;
	}

//	@Override
	protected void populate() { //throws IntegrationAdminException {
		try {
		List<IntSvrModel> intSvrModels = intNodeModel.getIntSvrModels();
		for (IntSvrModel intSvrModel : intSvrModels) {
			ExecutionGroupProxy intSvrProxy = new ExecutionGroupProxy(this, intSvrModel);
			intSvrs.add(intSvrProxy);
		}
		} catch (IntegrationAdminException e) {
			throw new RuntimeException("Error finding Int server Models", e);
		}
	}
	
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		return true; // FIXME complete this
	}

	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "BrokerProxy [name =" + getName() + "]"; 
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}
}
