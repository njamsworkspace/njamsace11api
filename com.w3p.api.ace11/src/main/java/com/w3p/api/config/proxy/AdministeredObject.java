package com.w3p.api.config.proxy;

public abstract class AdministeredObject {
	

	public AdministeredObject() {
	}

	
	public abstract String getName() throws ConfigManagerProxyPropertyNotInitializedException;

	
	/**
	 * Find all instances of a specific child-type of this ACE proxy class.<br>
	 *   Wrap in the equivalent w3p object<br>
	 *   Add to the 'typesMap' parameter 
	 * @param <T>            - <T extends AdministeredObject> i.e a subclass such as Application, RestApi
	 * @param typesMap       - Map to be populated
	 * @param thisAceProxy   - The ACE proxy that this class wraps (or adapts)
	 * @param aceTypeClass   - com.ibm.integration.admin.proxy.XxxxProxy.class
	 * @param childProxyClazz - XxxxProxy.class 
	 * @param iibMethod      - getAllXxxx
	 */

	public boolean hasBeenPopulatedByBroker() {
		return true;
	}
}
