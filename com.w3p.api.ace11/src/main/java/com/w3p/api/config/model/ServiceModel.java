package com.w3p.api.config.model;

import com.fasterxml.jackson.databind.JsonNode;

public class ServiceModel extends DeployableModel {
	

	public ServiceModel(IntSvrModel intSvrModel, JsonNode serviceNode) {
		super(intSvrModel, serviceNode);
	}
	
	public ServiceModel create() {
		return (ServiceModel) super.create();
	}
	
	@Override
	public String toString() {
		return "ServiceModel [getName()=" + getName() + "]";
	}	
}
