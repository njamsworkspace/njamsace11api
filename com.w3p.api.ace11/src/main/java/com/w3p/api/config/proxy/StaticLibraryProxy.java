package com.w3p.api.config.proxy;

import com.w3p.api.config.model.StaticLibModel;


public class StaticLibraryProxy extends DeployableProxy {


	public StaticLibraryProxy(DeployableProxy intSvrProxy, StaticLibModel stLibModel) {
		super(intSvrProxy, stLibModel);
	}

	public String getUri() {
		return ((StaticLibModel)deployableModel).getUri();
	}

	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "StaticLibraryProxy [" + getName() + "]";  
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}
}
