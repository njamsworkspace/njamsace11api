package com.w3p.api.config.model;

import com.fasterxml.jackson.databind.JsonNode;

public class AppModel extends DeployableModel {
	

	public AppModel(IntSvrModel intSvrModel, JsonNode appNode) {
		super(intSvrModel, appNode);
	}
	
	public AppModel create() {
		return (AppModel) super.create();
	}
	
	@Override
	public String toString() {
		return "AppModel [getName()=" + getName() + "]";
	}	
}
