package com.w3p.api.config.model;

import com.fasterxml.jackson.databind.JsonNode;

public class StaticLibModel extends DeployableModel {

	public StaticLibModel(DeployableModel intSvrModel, JsonNode stLibNode) { //IntSvrModel intSvrModel, JsonNode stLibNode) {
		super(intSvrModel, stLibNode);
	}
	
	public StaticLibModel create() {
		return (StaticLibModel) super.create();
	}

	@Override
	public String toString() {
		return "SharedLibModel [getName()=" + getName() + "]";
	}	
}
