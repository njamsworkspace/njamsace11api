package com.w3p.api.config.proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.w3p.api.config.model.AppModel;
import com.w3p.api.config.model.IntSvrModel;
import com.w3p.api.config.model.RestApiModel;
import com.w3p.api.config.model.ServiceModel;
import com.w3p.api.config.model.SharedLibModel;


public class ExecutionGroupProxy extends DeployableProxy { //AdministeredObject {
	private final AdministeredObject brokerProxy;
	private final IntSvrModel intSvrModel;
	
	// Applications
//	private final Map<String, ApplicationProxy> appProxiesMap = new LinkedHashMap<>();
	private final List<DeployableProxy> appProxies = new LinkedList<>();
	// Rest Apis
//	private final Map<String, RestApiProxy> restApiProxiesMap = new LinkedHashMap<>();
	private final List<DeployableProxy> restApiProxies = new LinkedList<>();
	// Services (SOAP)
//	private final Map<String, ServiceProxy> serviceProxiesMap = new LinkedHashMap<>();
	private final List<DeployableProxy> serviceProxies = new LinkedList<>();	
	// Shared Libraries
//	private final Map<String, SharedLibraryProxy> sharedLibProxiesMap = new LinkedHashMap<>();
	private final List<SharedLibraryProxy> sharedLibProxies = new LinkedList<>();

	
	public ExecutionGroupProxy(AdministeredObject brokerProxy, IntSvrModel intSvrModel) {
		this.brokerProxy = brokerProxy;
		this.intSvrModel = intSvrModel;
	}
	
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		return intSvrModel != null ? intSvrModel.getName() : null; 
	}

	// Applications
	public Enumeration<DeployableProxy> getApplications(Properties running) { //throws IntegrationAdminException {
		populateAppProxies();
		return Collections.enumeration(appProxies);
	}	
	private void populateAppProxies() {
		if (appProxies.isEmpty()) {
			populateApps();
		}		
	}
	protected void populateApps() {
		List<AppModel> appModels =  intSvrModel.getAppModels();
		for (AppModel appModel : appModels) {
			ApplicationProxy appProxy = new ApplicationProxy(this, appModel); // (DeployableProxy)
			appProxies.add(appProxy);
		}	
	}
	
	// Rest Apis
	public Enumeration<DeployableProxy> getRestApis(Properties running) { //throws IntegrationAdminException {
		populateRestApiProxies();
		return Collections.enumeration(restApiProxies);
	}	
	private void populateRestApiProxies() {
		if (restApiProxies.isEmpty()) {
			populateRestApis();
		}		
	}
	protected void populateRestApis() {
		List<RestApiModel> restApiModels = intSvrModel.getRestApiModels();
		for (RestApiModel restApiModel : restApiModels) {
			RestApiProxy restApiProxy = new RestApiProxy(this, restApiModel);
			restApiProxies.add(restApiProxy);
		}	
	}	
	
	// Services (SOAP)
	public Enumeration<DeployableProxy> getServices(Properties running) { //throws IntegrationAdminException {
		populateServiceProxies();
		return Collections.enumeration(serviceProxies);
	}	
	private void populateServiceProxies() {
		if (serviceProxies.isEmpty()) {
			populateServices();
		}		
	}
	protected void populateServices() {
		List<ServiceModel> serviceModels = intSvrModel.getServiceModels();
		for (ServiceModel serviceModel : serviceModels) {
			ServiceProxy serviceProxy = new ServiceProxy(this, serviceModel);
			serviceProxies.add(serviceProxy);
		}	
	}	
	
	// Shared Libraries
	public Enumeration<SharedLibraryProxy> getSharedLibraries(Properties running) { //throws IntegrationAdminException {
		// TODO Decide whether to handle only active Apps or just return all deployed.
		populateSharedLibProxies();
		return Collections.enumeration(sharedLibProxies);
	}
	private void populateSharedLibProxies() {
		if (sharedLibProxies.isEmpty()) {
			populateSharedLibs();;
		}		
	}
	protected void populateSharedLibs() {
		List<SharedLibModel> libModels = intSvrModel.getSharedLibModels();
		for (SharedLibModel sharedLibModel : libModels) {
			SharedLibraryProxy sharedLibProxy = new SharedLibraryProxy(this, sharedLibModel);
			sharedLibProxies.add(sharedLibProxy);
		}	
	}	

	public BrokerProxy getBrokerProxy() {
		return (BrokerProxy)brokerProxy;
	}
	
	/**
	 * This only exists to satisfy the reflection call in TopologyCreator#processChildren().
	 * An Integration Server is not the drect parent of subflows.
	 * @param properties 
	 * @return
	 */
	public Enumeration<SubFlowProxy> getSubFlows(Properties properties) { //throws IntegrationAdminException {
		return Collections.enumeration(new ArrayList<SubFlowProxy>());
	}
	
	
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		return true; // FIXME
	}

	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "ExecutionGroupProxy [brokerProxy=" + brokerProxy.getName() + ", intSvrProxy=" + getName() + "]"; 
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}
}
