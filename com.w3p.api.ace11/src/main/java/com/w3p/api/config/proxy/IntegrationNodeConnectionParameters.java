package com.w3p.api.config.proxy;

public class IntegrationNodeConnectionParameters implements BrokerConnectionParameters {
	
	private final String integrationNodeHost;
	private final int integrationNodePort;
	private final String username;
	private final String password;
	private final boolean useSSL;


	public IntegrationNodeConnectionParameters(String integrationNodeHost, int integrationNodePort) {
		this(integrationNodeHost, integrationNodePort, "", "");
	}
	
	public IntegrationNodeConnectionParameters(String integrationNodeHost, int integrationNodePort, String username, String password) {
		this(integrationNodeHost, integrationNodePort, username, password, false);
	}
	
	/**
	 * Connects, using the parameters to the web administration port
	 * on which an integration node is listening on the named host.
	 * @param integrationNodeHost The host name of the integration nod 
	 * @param integrationNodePort The web administration port number of the Int node
	 * @param username (default is "")
	 * @param password (default is "")
	 * @param useSSL  (default is 'false')
	 */
	public IntegrationNodeConnectionParameters(String integrationNodeHost, int integrationNodePort, String username, String password, boolean useSSL) {
		this.integrationNodeHost = integrationNodeHost;
		this.integrationNodePort = integrationNodePort;
		this.username = username;
		this.password = password;
		this.useSSL = useSSL;
	}

	public String getIntegrationNodeHost() {
		return integrationNodeHost;
	}

	public int getIntegrationNodePort() {
		return integrationNodePort;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public boolean isUseSSL() {
		return useSSL;
	}



}
