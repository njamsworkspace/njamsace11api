package com.w3p.api.config.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.MissingNode;

public class FlowModel extends DeployableModel {
	protected final DeployableModel parentModel;
	protected JsonNode flowJsonNode;

	public FlowModel(DeployableModel parentModel) {
		this.parentModel = parentModel;
		super.httpClient = parentModel.getHttpClient();
	}
	
	public String getName() { // throws IIB exception - check that 'Missing' class not returned
		if (name == null) {
			JsonNode nameNode = flowJsonNode.path(NAME);
			if (nameNode instanceof MissingNode) {
//				throw new BrokerProxyException;				
			}
			name = nameNode.asText();
		}
		return name;
	}

}
