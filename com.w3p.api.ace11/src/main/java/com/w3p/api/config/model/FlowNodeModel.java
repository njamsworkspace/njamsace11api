package com.w3p.api.config.model;

import java.awt.Point;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.ibm.integration.admin.http.HttpResponse;
import com.w3p.api.config.proxy.ConfigManagerProxyLoggedException;


public class FlowNodeModel extends AbstractModel {
	private static final Logger logger = LoggerFactory.getLogger(FlowNodeModel.class);
	
	private final FlowModel parentFlowModel;
	private JsonNode thisNode;	
	private boolean hasChildren;
	private String type ;

	protected String messageFlowBody;
	private Point location; // = new Point();
	private JsonNode connectionsNode; // A JsonArrayNode -each element being a link to or from this node
	private Properties nodeProperties = new Properties();

	
	public FlowNodeModel(FlowModel parentFlowModel) {
		super.httpClient = parentFlowModel.getHttpClient();
		this.parentFlowModel = parentFlowModel; 
	}
	
	public FlowNodeModel create(JsonNode flowNodeNode) {
		this.thisNode = flowNodeNode;
		return this;
	}
	
	public FlowNodeModel populate() { 
		try {
			hasChildren = thisNode.path("hasChildren").asBoolean();
			String nodeName = thisNode.path("name").asText();
			if (nodeName.contains("#InTerminal")) {
				name = StringUtils.substringAfterLast(nodeName, "."); // This as a Subflow Input node
			} else if (nodeName.contains("#OutTerminal")) {
				name = StringUtils.substringAfterLast(nodeName, "."); // This as a Subflow Output node
			} else {
				name = nodeName;
			}
			String className = thisNode.path("descriptiveProperties").path("className").asText();
			if (className.equals("SubFlowNode")) {
				type =  className;
				
				/* Get the actual location (parent container and subflow) by calling the 'instance' uri */
				String instanceURI = thisNode.path("children").path("subFlow").path("uri").asText();
				/* Must get the link from here to find the actual location */
				HttpResponse response = super.httpClient.getMethod(instanceURI);
				if (response.getStatusCode() == 200) {
					String respBody = response.getBody();
					setPropertySubflowUri(respBody);
				} else {
					String msg = String.format("HTTP error '%d' when getting the subflow URI for subflow node '%s' in parent '%s'", 
							response.getStatusCode(), name, parentFlowModel.getName());
					logger.error(msg);
					throw new ConfigManagerProxyLoggedException(msg);
				}
				
			} else if (className.endsWith("Node")) {
				type = className;
			} else {
				type =  String.format("%sNode", className); // Add 'Node' to node names to match IIB names				
			}
			
			// Set the 'x,y' co-ords
			String locationAsString = thisNode.path("descriptiveProperties").path("location").asText(); // "x,y"
			String[] co_ords = locationAsString.split(",");
			location = new Point(Integer.parseInt(co_ords[0]), Integer.parseInt(co_ords[1]));
			connectionsNode = thisNode.path("links"); // ArrayNode of Connections
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		
		return this;
	}

	private void setPropertySubflowUri(String respBody) throws JsonProcessingException, JsonMappingException {
		JsonNode actualSubflow = mapper.readTree(respBody);
		String subflowURI = actualSubflow.path("links").get(0).path("uri").asText();
		nodeProperties.put("subflowURI", subflowURI); // Store as a property
	}

	public boolean hasChildren() {
		return hasChildren;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public Point getLocation() {
		return location;
	}
	public String getSubflowName(boolean xxx) {
		return null; // FIXME
	}

	public JsonNode getConnectionsNode() {
		if (connectionsNode == null) {
			populate();
		}
		return connectionsNode;
	}
	
	public Properties getProperties() {		
		return nodeProperties;
	}

	@Override
	public String toString() {
		return "FlowNodeModel [name=" + name + "]";
	}
}
