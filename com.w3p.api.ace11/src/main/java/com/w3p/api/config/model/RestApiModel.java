package com.w3p.api.config.model;

import com.fasterxml.jackson.databind.JsonNode;

public class RestApiModel extends DeployableModel {
	

	public RestApiModel(IntSvrModel intSvrModel, JsonNode apiNode) {
		super(intSvrModel, apiNode);
	}
	
	public RestApiModel create() {
		return (RestApiModel) super.create();
	}
	
	@Override
	public String toString() {
		return "AppModel [getName()=" + getName() + "]";
	}	
}
