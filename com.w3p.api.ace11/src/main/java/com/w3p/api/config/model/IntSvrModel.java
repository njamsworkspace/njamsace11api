package com.w3p.api.config.model;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.ibm.integration.admin.http.HttpClient;
import com.ibm.integration.admin.http.HttpResponse;


public class IntSvrModel extends DeployableModel {
	private final JsonNode intSvrLinkNode;
	private JsonNode intSvrNode;
	
	private final List<AppModel> appModels = new LinkedList<>(); // Applications
	private final List<RestApiModel> restApiModels = new LinkedList<>(); // RestApis
	private final List<ServiceModel> serviceModels = new LinkedList<>(); // Services (SOAP)
	private final List<SharedLibModel> sharedLibModels = new LinkedList<>(); // Shared Libraries


	public IntSvrModel(HttpClient httpClient, JsonNode intSvrLinkNode) {
		super.httpClient = httpClient;
		this.intSvrLinkNode = intSvrLinkNode;
	}
	
	public IntSvrModel create() {		
		String intSvrUri = intSvrLinkNode.path(URI).asText();
		try {
			HttpResponse response = httpClient.getMethod(intSvrUri);
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error getting Integration Server details for URI '%s'. Status '%d', Message '%s'.",
						intSvrUri, response.getStatusCode(), response.getError() ));
			}
			
			intSvrNode = mapper.readTree(response.getBody());
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return this;
	}
	
	public void populateApps() {		
		String appsUri = intSvrNode.path(CHILDREN).path("applications").path(URI).asText();
		try {
			HttpResponse response = httpClient.getMethod(appsUri);
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error getting Applications link for URI '%s'. Status '%d', Message '%s'.",
						appsUri, response.getStatusCode(), response.getError() ));
			}
			
			JsonNode appsNode = mapper.readTree(response.getBody());
			JsonNode appArrayNode = appsNode.path(CHILDREN);
			for (JsonNode appNode : appArrayNode) {
				AppModel appModel = new AppModel(this, appNode);
				appModel.create();
				if (appModel.isRunning()) {					
					appModels.add(appModel);
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}	
	}
	
	public void populateRestApis() {		
		String appsUri = intSvrNode.path(CHILDREN).path("restApis").path(URI).asText();						
		try {
			HttpResponse response = httpClient.getMethod(appsUri);
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error getting Rest Apis link for URI '%s'. Status '%d', Message '%s'.",
						appsUri, response.getStatusCode(), response.getError() ));
			}
			
			JsonNode restApisNode = mapper.readTree(response.getBody());
			JsonNode restApiArrayNode = restApisNode.path(CHILDREN);
			for (JsonNode restApiNode : restApiArrayNode) {
				RestApiModel restApiModel = new RestApiModel(this, restApiNode);
				restApiModel.create();
				if (restApiModel.isRunning()) {					
					restApiModels.add(restApiModel);
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}	
	}
	
	public void populateServices() {		
		String servicesUri = intSvrNode.path(CHILDREN).path("services").path(URI).asText();						
		try {
			HttpResponse response = httpClient.getMethod(servicesUri);
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error getting Sewrvices link for URI '%s'. Status '%d', Message '%s'.",
						servicesUri, response.getStatusCode(), response.getError() ));
			}
			
			JsonNode servicesNode = mapper.readTree(response.getBody());
			JsonNode servicesArrayNode = servicesNode.path(CHILDREN);
			for (JsonNode serviceNode : servicesArrayNode) {
				ServiceModel serviceModel = new ServiceModel(this, serviceNode);
				serviceModel.create();
				if (serviceModel.isRunning()) {					
					serviceModels.add(serviceModel);
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}	
	}
	
	public void populateSharedLibs() {		
		String libsUri = intSvrNode.path(CHILDREN).path("sharedLibraries").path(URI).asText();						
		try {
			HttpResponse response = httpClient.getMethod(libsUri);
			if (response.getStatusCode() != 200) {
				throw new RuntimeException(String.format("Error getting Applications link for URI '%s'. Status '%d', Message '%s'.",
						libsUri, response.getStatusCode(), response.getError() ));
			}
			
			JsonNode libsNode = mapper.readTree(response.getBody());
			JsonNode libsArrayNode = libsNode.path(CHILDREN);
			for (JsonNode libNode : libsArrayNode) {
				SharedLibModel libModel = new SharedLibModel(this, libNode);
				libModel.create();
				sharedLibModels.add(libModel);
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}	
	}
	public String getName() {
		if (name == null) {
			name = intSvrNode != null ? intSvrNode.path(NAME).asText() : null;
		}
		return name;
	}
	
	public boolean isRunning() {
		return intSvrLinkNode.path("active").path("isRunning").asBoolean();
	}
	
	public List<AppModel> getAppModels() {
		if (appModels.isEmpty()) {
			populateApps();
		}
		return appModels;
	}
	
	public List<RestApiModel> getRestApiModels() {
		if (restApiModels.isEmpty()) {
			populateRestApis();
		}
		return restApiModels;
	}
	
	public List<ServiceModel> getServiceModels() {
		if (serviceModels.isEmpty()) {
			populateServices();
		}
		return serviceModels;
	}
	
	public List<SharedLibModel> getSharedLibModels() {
		if (sharedLibModels.isEmpty()) {
			populateSharedLibs();
		}
		return sharedLibModels;
	}

	@Override
	public String toString() {
		return "IntSvrModel [getName()=" + getName() + "]";
	}
}
