package com.w3p.api.config.proxy;

import com.w3p.api.config.model.SharedLibModel;


public class SharedLibraryProxy extends DeployableProxy {


	public SharedLibraryProxy(DeployableProxy intSvrProxy, SharedLibModel shLibModel) {
		super(intSvrProxy, shLibModel);
	}

	@Override
	public String toString() {
		String toString = null;
		try {
			toString = "SharedLibraryProxy [" + getName() + "]";  
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			// TODO: handle exception
		}
		return toString;
	}
}
