package com.w3p.api.config.proxy;

public class ConfigManagerProxyLoggedException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConfigManagerProxyLoggedException() {
	}

	public ConfigManagerProxyLoggedException(String message) {
		super(message);
	}

	public ConfigManagerProxyLoggedException(Throwable cause) {
		super(cause);
	}

	public ConfigManagerProxyLoggedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConfigManagerProxyLoggedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
