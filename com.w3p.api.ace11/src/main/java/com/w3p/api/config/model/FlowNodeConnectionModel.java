package com.w3p.api.config.model;

public class FlowNodeConnectionModel {
	private final FlowNodeModel sourceNode;
	private final String sourceOuputTerminal;
	private final FlowNodeModel targetNode;	 
	private final String targetInputTerminal;
	

	public FlowNodeConnectionModel(FlowNodeModel sourceNode, String sourceOuputTerminal, FlowNodeModel targetNode, String targetInputTerminal) {
		this.sourceNode = sourceNode;
		this.sourceOuputTerminal = sourceOuputTerminal;
		this.targetNode = targetNode;
		this.targetInputTerminal = targetInputTerminal;
	}
	public FlowNodeModel getSourceNode() {
		return sourceNode;
	}
	public FlowNodeModel getTargetNode() {
		return targetNode;
	}
	public String getSourceOuputTerminal() {
		return sourceOuputTerminal;
	}
	public String getTargetInputTerminal() {
		return targetInputTerminal;
	}
	public String getSourceNodeName() {
		return sourceNode.getName();
	}
	@Override
	public String toString() {
		return "FlowNodeConnectionModel [sourceNode=" + sourceNode + ", sourceOuputTerminal=" + sourceOuputTerminal
				+ ", targetNode=" + targetNode + ", targetInputTerminal=" + targetInputTerminal + "]";
	}

}
