package com.w3p.api.config.proxy;

public abstract class FlowProxy extends AdministeredObject {
	
	protected final AdministeredObject adminObjectProxy;

	
	public FlowProxy(AdministeredObject adminObjectProxy) {
		this.adminObjectProxy = adminObjectProxy;
	}
	
	public abstract boolean isRunning()  throws ConfigManagerProxyPropertyNotInitializedException; 

	protected abstract void populate(); //{ // throws IntegrationAdminException {
}
